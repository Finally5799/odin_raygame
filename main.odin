package main

import raylib "vendor:raylib"

main :: proc() {
    screenWidth, screenHeight: i32 = 800, 450

    raylib.InitWindow(screenWidth, screenHeight, "RayLib")
    raylib.SetTargetFPS(60)

    for !raylib.WindowShouldClose() {
        raylib.BeginDrawing()
        raylib.ClearBackground(raylib.RAYWHITE)
        raylib.DrawText("Congrats! You created your first window!", 190, 200, 20, raylib.LIGHTGRAY)
        raylib.EndDrawing()
    }

    raylib.CloseWindow()
}